; Quickly download all the requirements for this theme:
;
;   cd /path/to/drupal/root
;   drush make --no-core sites/all/themes/sassy_framework/sassy_framework.make ./
;
; NOTE: Until http://drupal.org/node/1539076 is fixed, drush make WILL DELETE
;       YOUR sites/all DIRECTORY! So run this make file before adding other projects
;       or themes to your Drupal install.
;

api = 2

projects[] = sassy
projects[] = prepro
projects[] = libraries

libraries[phpsass][download][type] = git
libraries[phpsass][download][url] = git://github.com/richthegeek/phpsass.git
libraries[phpsass][destination] = "libraries"