Sassy Framework -- A basic theme based on Framework using SASS

Dependencies:
  - Sassy (http://drupal.org/project/sassy)
    Sassy's dependencies are:
      - Libraries (http://www.drupal.org/project/libraries)
      - PrePro (http://www.drupal.org/project/prepro)
      - PHPSass compiler (https://github.com/richthegeek/phpsass)
    See the install instructions for Sassy: http://livelinknewmedia.com/documents/Making%20Drupal%20Sassy.pdf


